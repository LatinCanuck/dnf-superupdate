# DNF SuperUpdate

DNF SuperUpdate is a powerful bash script designed for Fedora 38 and above that streamlines the process of system updates. The script is crafted to handle package updates, cleanup of old packages, handling leftover RPM configuration files, updating Flatpak packages, and cleaning up cached package data. 

## Usage

You can download, make executable, and run the script on your Fedora system using a single command:

```bash
curl -o dnf-superupdate.sh "https://gitlab.com/LatinCanuck/dnf-superupdate/-/raw/master/dnf-superupdate.sh" && chmod +x dnf-superupdate.sh && sudo ./dnf-superupdate.sh
```
## License

DNF SuperUpdate is licensed under the MIT License. This means you can use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the software. Please see the LICENSE file for the full text of the MIT License.

## Disclaimer

Always be careful when downloading scripts from the internet and running them with sudo privileges. Please review the contents of the script before executing it.

## Contributing

We welcome contributions to the DNF SuperUpdate project! Feel free to submit pull requests or open issues on our GitLab page.
Support

If you need help with DNF SuperUpdate, please open an issue on our GitLab page. I'll do our best to help you out!
